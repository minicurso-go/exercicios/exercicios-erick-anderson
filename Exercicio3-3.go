package main

import "fmt"

func main() {
	var num int

	fmt.Println("Digite um número:")
	fmt.Scan(&num)

	for i := 1; i < 11; i++ {
		fmt.Println(num, "x", i, "=", i*num)
	}
}
