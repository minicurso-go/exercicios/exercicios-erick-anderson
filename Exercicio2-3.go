package main

import (
	"fmt"
)

func main() {
	var a float64
	var b float64
	var c float64
	pi := 3.14159

	fmt.Println("Digite os valores de a, b e c:")
	fmt.Scan(&a, &b, &c)

	areaTriangulo := (a * c) / 2
	fmt.Println("Área do triângulo retângulo: %.4f\n", areaTriangulo)

	areaCirculo := pi * c * c
	fmt.Println("Área do círculo: %.4f\n", areaCirculo)

	areaTrapezio := ((a + b) * c) / 2
	fmt.Println("Área do trapézio: %.4f\n", areaTrapezio)

	areaQuadrado := b * b
	fmt.Println("Área do quadrado: %.4f\n", areaQuadrado)

	areaRetangulo := a * b
	fmt.Println("Área do retângulo: %.4f\n", areaRetangulo)
}
