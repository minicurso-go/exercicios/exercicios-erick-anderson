package main

import "fmt"

func converterTemperatura(temperatura float64, escolha string) float64 {
	if escolha == "C" {
		return (temperatura * 9 / 5) + 32
	} else {
		return (temperatura - 32) * 5 / 9
	}
}

func main() {
	var temperatura float64
	var escolha string

	fmt.Println("Digite 'C' para converter de Celsius para Fahrenheit, ou 'F' para converter de Fahrenheit para Celsius:")
	fmt.Scanln(&escolha)

	fmt.Println("Digite a temperatura a ser convertida:")
	fmt.Scanln(&temperatura)

	resultado := converterTemperatura(temperatura, escolha)

	if escolha == "C" {
		fmt.Println("Fahrenheit", resultado)
	} else {
		fmt.Println("Celsius", resultado)
	}
}
