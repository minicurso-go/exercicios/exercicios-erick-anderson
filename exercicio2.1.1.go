package main

import (
	"fmt"
)

func main() {
	var x, y float32
	fmt.Println("Digite as coordenadas x e y do ponto:")
	fmt.Scan(&x, &y)

	if x == 0 && y == 0 {
		fmt.Println("Ponto central")
	} else if x == 0 {
		fmt.Println("Linha Y")
	} else if y == 0 {
		fmt.Println("Linha X")
	} else if x > 0 && y > 0 {
		fmt.Println("1º Quadrante")
	} else if x < 0 && y > 0 {
		fmt.Println("2º Quadrante ")
	} else if x < 0 && y < 0 {
		fmt.Println("3º Quadrante")
	} else if x > 0 && y < 0 {
		fmt.Println("4º Quadrante")
	}
}
