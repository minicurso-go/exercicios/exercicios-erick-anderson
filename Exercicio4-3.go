package main

import "fmt"

func calcularMedia(numeros []float64) float64 {
	total := 0.0
	for _, valor := range numeros {
		total += valor
	}
	return total / float64(len(numeros))
}

func main() {
	var a float64
	var b float64
	var c float64
	var d float64

	fmt.Println("Digite o valor de a: ")
	fmt.Scanln(&a)
	fmt.Println("Digite o valor de b: ")
	fmt.Scanln(&b)
	fmt.Println("Digite o valor de c: ")
	fmt.Scanln(&c)
	fmt.Println("Digite o valor de d: ")
	fmt.Scanln(&d)

	numeros := []float64{a, b, c, d}

	media := calcularMedia(numeros)

	fmt.Printf("A média é: %.2f\n", media)
}
