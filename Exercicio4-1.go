package main

import "fmt"

func main() {
	var num1 int
	var num2 int

	fmt.Print("Digite o primeiro número: ")
	fmt.Scanln(&num1)

	fmt.Print("Digite o segundo número: ")
	fmt.Scanln(&num2)

	resultado := soma(num1, num2)
	fmt.Printf("A soma de %d e %d é %d\n", num1, num2, resultado)
}

func soma(num1 int, num2 int) int {
	return num1 + num2
}
