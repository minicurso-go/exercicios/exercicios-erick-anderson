package main

import "fmt"

func calcularPotencia(base int, expoente int) int {
	resultado := 1
	for i := 0; i < expoente; i++ {
		resultado *= base
	}
	return resultado
}

func main() {
	var base, expoente int

	fmt.Println("Digite a base: ")
	fmt.Scan(&base)

	fmt.Println("Digite o expoente: ")
	fmt.Scan(&expoente)

	if expoente < 0 {
		fmt.Println("Por favor, digite um expoente não-negativo.")
		return
	}

	resultado := calcularPotencia(base, expoente)
	fmt.Printf("%d elevado a %d é %d\n", base, expoente, resultado)
}
