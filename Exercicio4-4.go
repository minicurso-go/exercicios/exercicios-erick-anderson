package main

import "fmt"

func fatorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * fatorial(n-1) // Chamada recursiva
}

func main() {
	var numero int
	fmt.Println("Digite um número para calcular o fatorial: ")
	fmt.Scan(&numero)

	if numero < 0 {
		fmt.Println("Número inválido. Por favor, digite um número inteiro não-negativo.")
		return
	}

	resultado := fatorial(numero)
	fmt.Printf("O fatorial de %d é %d\n", numero, resultado)
}
