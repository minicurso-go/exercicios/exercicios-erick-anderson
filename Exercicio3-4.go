package main

import "fmt"

func main() {

	for i := 1; i < 101; i++ {
		if i%5 == 0 && i%3 == 0 {
			fmt.Println("FrizzBuzz")
		} else if i%5 == 0 {
			fmt.Println("Buzz")
		} else if i%3 == 0 {
			fmt.Println("Frizz")
		} else {
			fmt.Println(i)
		}

	}
}
