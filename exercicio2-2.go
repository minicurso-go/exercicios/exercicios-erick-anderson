package main

import (
	"fmt"
)

func main() {
	var num1 float64
	var num2 float64
	var operacao int
	
	fmt.Println("Digite dois números:")
	fmt.Scanln(&num1, &num2)

	fmt.Println("Escolha uma operação:1-soma 2-subtração 3-Divisão 4-Multiplicação")
	fmt.Scanln(&operacao)

	switch operacao {
	case 1:
		fmt.Printf("Resultado: ", num1+num2)
	case 2:
		fmt.Printf("Resultado: ", num1-num2)
	case 3:
		if num2 != 0 {
			fmt.Printf("Resultado:", num1/num2)
		} else {
			fmt.Println("Não se divide por 0.")
		}
	case 4:
		fmt.Printf("Resultado:", num1*num2)
	default:
		fmt.Println("Operação não encontrada.")
	}
}
