package main

import "fmt"

func procurarElemento(lista []int, elemento int) (int, bool) {
	for i, valor := range lista {
		if valor == elemento {
			return i, true
		}
	}
	return -1, false
}

func main() {
	lista := []int{5, 10, 15, 20, 25, 30, 35, 40}
	var elemento int

	fmt.Print("Digite o elemento que deseja procurar (5, 10, 15, 20, 25, 30, 35, 40): ")
	fmt.Scan(&elemento)

	posicao, encontrado := procurarElemento(lista, elemento)
	if encontrado {
		fmt.Println("Elemento encontrado na posição", posicao)
	} else {
		fmt.Println("Elemento não encontrado na lista.")
	}
}
